function string5(test_array){

    if(test_array==[]){
        return ""
    }

    let newArray= []
   
    for(let i=0;i<test_array.length;i++){

        if(test_array[i]!="" || test_array[i]!=undefined || test_array[i]!= null ){

            newArray.push(test_array[i]) // if not  empty string push
        }


        newArray = newArray.filter(i=>i)  //remove null,empty,undefined

    }

    if (newArray && newArray!=[""] ){ //if not empty string array

        return newArray.join(" ");

    }

    else{
        return "" //return empty string
    }

}

module.exports = string5;