const string4 = require('../string4');


let testObj = { "middle_name":"sEAn", "first_name":"jAcK","last_name":"haRriSoN"};  // enter first name,(middle) and lastname


console.log(string4(testObj)); //Jack Sean Harrison

testObj = { "first_name":"RyAn", "last_name":"rYnoLlED"};

console.log(string4(testObj)); // Ryan Rynolled


testObj = { "last_name":"cRUIse","first_name":"tOM" };

console.log(string4(testObj)); // Tom Cruise


testObj = { "first_name":" ", "last_name":null};  //invalid data

console.log(string4(testObj));      //return "Wrong Input! Plz Type Valid Name" 