function string4(test_Obj){

    let newArray = []

 
   try{ 
       
        newArray.push( test_Obj["first_name"] ? test_Obj["first_name"] : "")

        newArray.push( test_Obj["middle_name"] ? test_Obj["middle_name"] : "")

        newArray.push( test_Obj["last_name"] ? test_Obj["last_name"] : "")

     
        for(let j=0;j<newArray.length;j++){

            newArray[j] = newArray[j].toLowerCase() // [ 'jack', 'sean', 'harrison' ]
        }
        
        for(let j=0;j<newArray.length;j++){

            newArray[j] = newArray[j].charAt(0).toUpperCase() + newArray[j].slice(1) // convert first letter upperacase and concatenation from index 1

        }

        return  newArray.join(" ") // Jack Sean Harrison

    }

    catch{
        return "Wrong Input! Plz Type Valid Name"
    }
  
}


module.exports = string4;