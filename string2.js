
function string2(test_String){

    test_String = test_String.split(".")
    
    if(test_String.length!=4){
        return []
    } 
    
    let result = []

    for(let i=0;i<test_String.length;i++){

        
        if(isNaN(test_String[i])){

            return []   // returning Empty array  input if error 
        }

        
        else if ((Number(test_String[i]))>=0 && (Number(test_String[i]))<=255){  // 48 to 57 ASCII value for (0 to 9) 

            result.push(Number(test_String[i]))
    
        }
        else{
            return []
        }
    }
                 

    return(result)
}

module.exports = string2;
